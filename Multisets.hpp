/*==============================================================================
 * Project : rollingMedian
 * File    : Multisets.h
 * Labs    : Roboticians
 * Contact : manuel.yguel.robotics@gmail.com
 * date    : 01 Mai 2010
 *
 *==============================================================================
 *     (c) Copyright 2010, Roboticians, Cartographers team, all rights reserved.
 *==============================================================================
 *  Software License Agreement (BSD License)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of copyright holders nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *==============================================================================
 *     Description
 *==============================================================================
 * Compute a rolling median using a multisets approach
 *------------------------------------------------------------------------------
 */

/**
 * @author Manuel Yguel
 * @email manuel.yguel.robotics@gmail.com
 */

#pragma once

#include <cassert>
#include <set>
#include <vector>

namespace rollingMedian {

namespace internal {

template <class T,
          class COMPARE = std::less<T>,
          class ALLOCATOR = std::allocator<T>>
class Multisets
{
  public:
    typedef T Element;
    typedef COMPARE Compare;
    typedef ALLOCATOR Allocator;

  public:
    Multisets()
        : m_window_idx(0)
    {
    }

  public:
    inline size_t windowSize() const
    {
        return m_window.size();
    }

    inline void windowSize(const size_t value)
    {
        m_window.resize(value);
        m_half_window = value / 2 + 1;
    }

    inline size_t windowIndex() const
    {
        return m_window_idx;
    }

  public:
    void update(const T &t)
    {
        const T &old = m_window[m_window_idx];
        const T &median = *m_above.begin(); //< exact for odd window sizes
        if (old < median)
        { // old is in m_below
            m_below.erase(m_below.find(old));
        }
        else
        { // old is in m_above
            m_above.erase(m_above.find(old));
        }

        if (m_compare(t, median))
        { // t < median
            // insert t into m_below
            m_below.insert(t);
        }
        else
        { // median <= t
            // insert t into m_above
            m_above.insert(t);
        }

        if (m_above.size() < m_half_window)
        {
            m_above.insert(popLastElement(m_below));
        }
        else if (m_above.size() > m_half_window)
        {
            m_below.insert(popFirstElement(m_above));
        }

        m_window[m_window_idx] = t;
        m_window_idx = (m_window_idx + 1) % m_window.size();
    }

    void init(const T &t)
    {
        assert(m_above.size() + m_below.size() < m_window.size());
        if (0 == m_window_idx)
        {
            m_above.insert(t);
        }
        else
        {
            const T &median = *m_above.begin(); //< exact for odd window sizes
            if (m_compare(t, median))
            { // t < median
                // insert t into m_below
                m_below.insert(t);
            }
            else
            { // median <= t
                // insert t into m_above
                m_above.insert(t);
            }

            const size_t current_half = (m_window_idx + 1) / 2 + 1;
            if (m_above.size() < current_half)
            {
                m_above.insert(popLastElement(m_below));
            }
            else if (m_above.size() > current_half)
            {
                m_below.insert(popFirstElement(m_above));
            }
        }
        m_window[m_window_idx] = t;
        m_window_idx = (m_window_idx + 1) % m_window.size();
    }

  public:
    template <class OSTREAM>
    OSTREAM &print(OSTREAM &out) const
    {
        out << "Below\n\t";
        typename Multiset_t::const_iterator it, end = m_below.end();
        for (it = m_below.begin(); end != it; ++it)
        {
            out << *it << "  ";
        }
        out << "\nAbove\n\t";
        end = m_above.end();
        for (it = m_above.begin(); end != it; ++it)
        {
            out << *it << "  ";
        }
        out << "\n";
        return out;
    }

  public:
    inline T median() const
    {
        if (0 == ((m_above.size() + m_below.size()) % 2))
        {
            typename Multiset_t::const_iterator it = m_above.begin();
            T res = *it;
            ++it;
            res += *it;
            return res / T(2);
        }
        else
        {
            return *m_above.begin();
        }
    }

  protected:
    typedef std::multiset<T, Compare, Allocator> Multiset_t;

    inline void popFirstElement(Multiset_t &s, T &head) const
    {
        head = *(s.begin());
        s.erase(s.begin());
    }

    inline T popFirstElement(Multiset_t &s) const
    {
        T head;
        popFirstElement(s, head);
        return head;
    }

    inline void lastElement(Multiset_t &s, T &tail) const
    {
        return *(--(s.end()));
    }

    inline void popLastElement(Multiset_t &s, T &tail) const
    {
        typename Multiset_t::iterator it = s.end();
        --it;
        tail = *it;
        s.erase(it);
    }

    inline T popLastElement(Multiset_t &s) const
    {
        T tail;
        popLastElement(s, tail);
        return tail;
    }

  protected:
    Compare m_compare;
    Multiset_t m_below;
    Multiset_t m_above;
    std::vector<T> m_window;
    size_t m_window_idx;
    size_t m_half_window;
};

} // namespace internal

/** class Multisets to compute a rolling median using a multiset approach.
 * The requirements are as follow:
 *   - the elements should be comparable or a comparison functor is
 *     to be provided. One class that have the
 *          bool operator()( const T& e1, const T& e2 )
 *     method.
 *   - WARNING: two elements that compare equally: e1 < e2 and e2 < e1 are false
 *    ( e1 equal e2 ) are considered as equal and one or the other may be
 * returned by a call to the find function.
 *   - WARNING: for even value of the window size, it should be possible to sum
 * and divide by 2 T values.
 */
template <class T,
          bool ODD_WINDOW_SIZE_ONLY = true,
          class COMPARE = std::less<T>,
          class ALLOCATOR = std::allocator<T>>
class Multisets;

template <class T, class Compare, class Allocator>
class Multisets<T, true, Compare, Allocator>
    : public internal::Multisets<T, Compare, Allocator>
{
  protected:
    typedef internal::Multisets<T, Compare, Allocator> Base_t;
    using Base_t::m_above;
    using Base_t::median;

  public:
    using Base_t::windowSize;

  public:
    inline void windowSize(const size_t value)
    {
        if (1 == (value % 2))
        {
            Base_t::windowSize(value);
        }
    }

  public:
    inline T median() const
    {
        return *m_above.begin();
    }

    inline T initMedian() const
    {
        return Base_t::median();
    }
};

template <class T, class Compare, class Allocator>
class Multisets<T, false, Compare, Allocator>
    : public internal::Multisets<T, Compare, Allocator>
{
};

} // namespace rollingMedian
