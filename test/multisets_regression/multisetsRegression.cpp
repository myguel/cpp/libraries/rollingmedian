/*==============================================================================
 * Project : rollingMedian
 * File    : multisetRegression.cpp
 * Labs    : Roboticians
 * Contact : manuel.yguel.robotics@gmail.com
 * date    : 01 Mai 2010
 *
 *==============================================================================
 *     (c) Copyright 2010, Roboticians, Cartographers team, all rights reserved.
 *==============================================================================
 *  Software License Agreement (BSD License)
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of copyright holders nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *
 *==============================================================================
 *     Description
 *==============================================================================
 * Regression tests for computing a rolling median using a multisets approach
 *------------------------------------------------------------------------------
 */

/**
 * @author Manuel Yguel
 * @email manuel.yguel.robotics@gmail.com
 */

#include "Multisets.hpp"
#include <Eigen/Core>
#include <algorithm>
#include <gtest/gtest.h>

#include <boost/random/linear_congruential.hpp>
#include <boost/random/uniform_int.hpp>
#include <boost/random/variate_generator.hpp>

using namespace std;
using namespace rollingMedian;

boost::minstd_rand gen;
boost::uniform_int<> dist(1, 15);
const size_t NB_TRIALS = 42;

template <class CONTAINER>
void print(const CONTAINER &t)
{
    typename CONTAINER::const_iterator it, end = t.end();
    for (it = t.begin(); end != it; ++it)
    {
        cout << *it << "  ";
    }
    cout << endl;
}

TEST(ROLLING_MEDIAN___MULTISETS, odd_window_size_only)
{
    Multisets<double, true> median;
    const size_t wsize = 11;
    median.windowSize(wsize);

    std::vector<double> tab, aux;
    Eigen::VectorXd val(wsize);

    val.setRandom();
    for (size_t i = 0; i < wsize; ++i)
    {
        tab.push_back(val[i]);
        aux = tab;
        sort(aux.begin(), aux.end());
        median.init(val[i]);

        if (1 == ((i + 1) % 2))
        {
            EXPECT_EQ(aux[(i + 1) / 2], median.initMedian());
        }
        else
        {
            EXPECT_EQ(0.5 * (aux[i / 2] + aux[i / 2 + 1]), median.initMedian());
        }
    }

    for (size_t k = 0; k < NB_TRIALS; ++k)
    {
        val.setRandom();
        for (size_t i = 0; i < wsize; ++i)
        {
            tab[i] = val[i];
            aux = tab;
            sort(aux.begin(), aux.end());
            median.update(val[i]);

            EXPECT_EQ(aux[wsize / 2], median.median());
        }
    }
}

TEST(ROLLING_MEDIAN___MULTISETS, any_size)
{
    for (size_t ex = 0; ex < NB_TRIALS; ++ex)
    {
        Multisets<double, false> median;
        const size_t wsize = std::max(1, std::min(256, rand()));
        median.windowSize(wsize);

        std::vector<double> tab, aux;
        Eigen::VectorXd val(wsize);

        val.setRandom();
        for (size_t i = 0; i < wsize; ++i)
        {
            tab.push_back(val[i]);
            aux = tab;
            sort(aux.begin(), aux.end());
            median.init(val[i]);

            if (0 == (i % 2))
            {
                EXPECT_EQ(aux[(i + 1) / 2], median.median());
            }
            else
            {
                EXPECT_EQ(0.5 * (aux[i / 2] + aux[i / 2 + 1]), median.median());
            }
        }

        for (size_t k = 0; k < NB_TRIALS; ++k)
        {
            val.setRandom();
            for (size_t i = 0; i < wsize; ++i)
            {
                tab[i] = val[i];
                aux = tab;
                sort(aux.begin(), aux.end());
                median.update(val[i]);

                if (1 == (wsize % 2))
                {
                    EXPECT_EQ(aux[wsize / 2], median.median());
                }
                else
                {
                    EXPECT_EQ(0.5 * (aux[wsize / 2 - 1] + aux[wsize / 2]),
                              median.median());
                }
            }
        }
    }
}

void setRandomInt(std::vector<int> &val)
{
    for (size_t i = 0; i < val.size(); ++i)
    {
        val[i] = dist(gen);
    }
}

TEST(ROLLING_MEDIAN___MULTISETS, odd_window_size_only_repetitions)
{
    Multisets<double, true> median;
    const size_t wsize = 11;

    median.windowSize(wsize);

    std::vector<int> tab, aux;
    std::vector<int> val(wsize);

    setRandomInt(val);
    for (size_t i = 0; i < wsize; ++i)
    {
        tab.push_back(val[i]);
        aux = tab;
        sort(aux.begin(), aux.end());
        median.init(val[i]);

        if (1 == ((i + 1) % 2))
        {
            EXPECT_EQ(aux[(i + 1) / 2], median.initMedian());
        }
        else
        {
            EXPECT_EQ(0.5 * (aux[i / 2] + aux[i / 2 + 1]), median.initMedian());
        }
    }

    for (size_t k = 0; k < NB_TRIALS; ++k)
    {
        setRandomInt(val);
        for (size_t i = 0; i < wsize; ++i)
        {
            tab[i] = val[i];
            aux = tab;
            sort(aux.begin(), aux.end());
            median.update(val[i]);

            EXPECT_EQ(aux[wsize / 2], median.median());
        }
    }
}
