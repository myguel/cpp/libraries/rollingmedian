# rollingMedian

This is a header only implementation of rolling median in c++ for windows of odd or even size.

Example of use:
```cpp
#include "Multisets.hpp"
#include <iostream>

using namespace std;
using namespace rollingMedian;

Multisets<int, true> median;
const int wsize = 11;

int samples[20] = {0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
                   10, 11, 12, 13, 14, 15, 16, 17, 18, 19};

int main()
{
    median.windowSize(wsize);

    int i = 0;
    for (; i < wsize; ++i)
    {
        median.init(samples[i]);
    }
    cout << "Median: " << median.median() << endl;
    for (; i < 20; ++i)
    {
        median.update(samples[i]);
        cout << "Median: " << median.median() << endl;
    }
}
```